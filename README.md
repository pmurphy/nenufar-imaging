A collection of scripts I use when doing interferometry with NenuFAR.
- `update_parset.py` generates parset files and runs them with `DPPP`. `multi_cal.sh` is an example of how to run it with multiple subbands.
- `get_sizescale_for_wsclean.py` determines the appropriate size and scale parameters for `WSClean`. Example usage:
```python get_sizescale_for_wsclean.py SUN-2024-03-10/SB410.MS 512```
- `time_to_wsclean_interval.py` converts a UTC timestamp to an interval to be used with `WSClean`. 
Example usage:
```time_to_wsclean_interval.py SUN-2024-03-10/SB410.MS/ --trange 2024-03-10T12:13:00 2024-03-10T12:18:00```
- `generic_wsclean.sh` is a template script to run wsclean on a given measurement set (MS) with the parameters determined from the scripts above.
- `run_wsclean.sh` and other versions are specific calls to `generic_wsclean.sh` for a given MS.
- `plot_fits.py` will plot the fits file output of `WSClean`.
- `radio_euv_overlay.py` overlays the fits as contours on an AIA image. This one is the most finicky of all, I can only apologise. Example usage:
```
python scripts/radio_euv_overlay.py -s /databf/nenufar-tf/LT11/2024/03/20240310_101000_20240310_135000_SUN_TRACKING/SUN_TRACKING_20240310_101036_1.spectra --trange 2024-03-10T12:13:00 2024-03-10T12:23:00 --frange 55 90 -n 1 -a radio_images/2024-03-10/SB*/burst-t0000-image.fits
```