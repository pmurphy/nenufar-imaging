#!/bin/bash

# A list of previously run WSClean commands. 
# Dates indicate date of observation.
# In general, mgain 0.8, briggs 0 and multiscale 
# are what I find to be best for imaging bursts.
# auto-mask and -auto-threshold are useful but can lead to issues.

#wsclean -mem 85 -no-reorder -no-update-model-required -mgain 0.8 -weight briggs 0  -size 1024 1024 -scale 1.95amin -pol I -data-column CORRECTED_DATA -taper-gaussian 90  -multiscale -auto-threshold 3 -niter 500 -intervals-out 2789 -name SUN-19-5-2022/SB106_images/sun_19_05_2022 SUN-19-5-2022/SB106_aw_av.MS/

#wsclean -mem 95  -no-reorder -no-update-model-required \
# -mgain 0.7 -weight briggs 0 -multiscale \
# -auto-mask 3 -auto-threshold 0.3   \
# -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
# -niter 2000 -channels-out 16 -intervals-out 1440 -interval  0 1440 \
# -fit-beam -name $3 $4

#2022-10-04
#wsclean -mem 95  -no-reorder -no-update-model-required \
# -mgain 0.7 -weight briggs 0 -multiscale \
# -auto-mask 3 -auto-threshold 0.3 \
# -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
# -niter 2000 -intervals-out 194 -interval  0 194 \
# -fit-beam -name $3 $4

#2022-10-11
# wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.7 -weight briggs 0 -multiscale \
#  -auto-mask 3 -auto-threshold 0.3 \
#  -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
#  -niter 10000 -channels-out 63 -intervals-out 70 -interval 238 308 \
#  -fit-beam -name $3 $4

#2022-11-11
# wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.8 -weight briggs 0 -multiscale \
#  -local-rms -local-rms-window 10 -auto-mask 3 -auto-threshold 0.1 \
#  -size $1 $1 -scale $2amin -pol I,V -data-column CORRECTED_DATA1 \
#  -niter 100000 -channels-out 64 -intervals-out 3 -interval 3 6 \
#  -fit-beam -name $3 $4

#full day lower time/frequency resolution
#  wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.8 -weight briggs 0 -multiscale \
#  -local-rms -local-rms-window 10 -auto-mask 3 -auto-threshold 0.1 \
#  -size $1 $1 -scale $2amin -pol I,V -data-column CORRECTED_DATA1 \
#  -niter 1000 -intervals-out 1656 \
#  -fit-beam -name $3 $4

#2023-03-17
# wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.7 -weight briggs -1 -multiscale \
#  -auto-mask 3 -auto-threshold 0.3 \
#  -size $1 $1 -scale $2amin -pol I,V -data-column CORRECTED_DATA \
#  -niter 10000 -channels-out 1 -intervals-out 6 -interval 33 39 \
#  -fit-beam -name $3 $4

# 2023-06-01
# wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.8 -weight briggs 0 -multiscale \
#  -local-rms -local-rms-window 10 -auto-mask 3 -auto-threshold 0.1 \
#  -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
#  -niter 100000 -intervals-out 119 -interval 0 119 \
#  -fit-beam -name $3 $4

# 2023-07-02
# wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.8 -weight briggs 0 -multiscale \
#  -local-rms -local-rms-window 10 -auto-mask 3 -auto-threshold 0.1 \
#  -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
#  -niter 100000 -intervals-out 15 -interval 273 288 \
#  -fit-beam -name $3 $4

 # 2022-07-11
#  wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.8 -weight briggs 0 -multiscale \
#  -local-rms -local-rms-window 10 -auto-mask 3 -auto-threshold 0.1 \
#  -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
#  -niter 100000 -intervals-out 446 -interval 0 446 \
#  -fit-beam -name $3 $4
# 2023-09-20
#  wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.8 -weight briggs 0 -multiscale \
#  -local-rms -local-rms-window 10 -auto-mask 3 -auto-threshold 0.1 \
#  -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
#  -niter 100000 -intervals-out 60 -interval 89 149 \
#  -fit-beam -name $3 $4

#  # 2024-02-15
#  wsclean -mem 95  -no-reorder -no-update-model-required \
#  -mgain 0.8 -weight briggs 0 -multiscale \
#  -auto-threshold 3 \
#  -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
#  -niter 100000 -intervals-out 10 -interval 100 110 \
#  -fit-beam -name $3 $4

  # 2024-02-15
 wsclean -mem 95  -no-reorder -no-update-model-required \
 -mgain 0.8 -weight briggs 0 -multiscale \
 -auto-mask 3 -auto-threshold 1 \
 -size $1 $1 -scale $2amin -pol I -data-column CORRECTED_DATA \
 -niter 100000 -intervals-out 596 \
 -fit-beam -name $3 $4