#!/usr/bin/env python
"""
Calculate appropriate `size` and `scale` parameters
for wsclean from a given nenufar MS.
Inputs: MS file, desired number of pixels
"""
import sys

import astropy.units as u

from casacore import tables
from nenupy.instru import MiniArray, NenuFAR

def get_size_scale(ms):
    with tables.table(ms + '/ANTENNA', ack=False) as tant:
        tants = tant.getcol('NAME')

    nf_ants = []
    for ant in tants:
        ant = ant.strip('NEN')
        ant = ant.replace('R', 'A')
        nf_ants.append(ant)

    with tables.table(ms + '/SPECTRAL_WINDOW', ack=False) as sw:
        freq = sw.getcol('REF_FREQUENCY')[0]*u.Hz

    nf = NenuFAR(include_remote_mas=True)[nf_ants]
    ma = MiniArray(index=0)
    beam = nf.angular_resolution(freq).to(u.arcmin)
    fov = ma.angular_resolution(freq).to(u.arcmin)
    scale = beam/(4*u.pix) #nyquist sample the beam
    size = fov/scale #how many pixels in fov
    return size, scale

if __name__ == "__main__":
    ms = sys.argv[1]
    req_pix = float(sys.argv[2])*u.pix
    size, scale = get_size_scale(ms)
    new_scale = (size*scale)/req_pix
    print("Size:{} Scale:{}".format(size, scale))
    print("For size of {} use scale {}".format(req_pix, new_scale))
