#!/bin/bash

for sb in 410 361 354 316 310;
do
    # this order is important
    # calibrate using the calibration observation made after the solar one
    python /data/mpearse/scripts/update_parset.py SUN-2024-03-10/CAL/SB${sb}_a.MS -c
    
    # calibrate using the calibration observation made before the solar one and then combine
    python /data/mpearse/scripts/update_parset.py SUN-2024-03-10/CAL/SB${sb}.MS -c --combine
    
    # apply the calibration to the solar observation using the combined calibration solution
    python /data/mpearse/scripts/update_parset.py /databf/nenufar-nri/LT11/2024/03/20240310_101000_20240310_135000_SUN_TRACKING/L1/SB${sb}.MS -t 2024-03-10T13:43:40 2024-03-10T13:43:50 -u --combine
   
    # apply the calibration using non-combined calibration solution
    # python /data/mpearse/scripts/update_parset.py /databf/nenufar-nri/LT11/2024/03/20240310_101000_20240310_135000_SUN_TRACKING/L1/SB${sb}.MS -t 2024-03-10T12:13:00 2024-03-10T12:23:00 -u

done
