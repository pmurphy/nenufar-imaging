#!/usr/bin/env python

"""
Input LOFAR image fits file and return overlay of
radio image on top of AIA 171.
This is a real mess and requires hardcoding 
the names of output directories and AIA file locations.
"""

import argparse
import datetime
import glob
import gc
import itertools
import os
import pdb
import warnings
warnings.filterwarnings("ignore")

from multiprocessing import Pool, Lock

import astropy.units as u
# import cmocean
import matplotlib
import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np
import sunpy.map

from astropy.coordinates import Angle, SkyCoord
from astropy.visualization import LogStretch
from astropy.visualization.mpl_normalize import ImageNormalize
from astropy.time import Time
from cycler import cycler
from matplotlib.gridspec import GridSpec
from matplotlib.patches import Ellipse
from pathlib import Path
from sunpy.coordinates import frames, Helioprojective
from sunpy.net import Fido, helioviewer, attrs as a

from nenupy.beamlet import SData
from nenupy.undysputed import Dynspec

from icrs_to_helio import icrs_to_helio
hv = helioviewer.HelioviewerClient()
cbtab_cycler = cycler(color=['#006BA4', '#FF800E', '#ABABAB', '#595959', '#5F9ED1', '#C85200', '#898989', '#A2C8EC', '#FFBC79', '#CFCFCF'])
matplotlib.rcParams['axes.prop_cycle'] = cbtab_cycler

def get_heliomaps(fits_file, nchans=4, cmap='Reds'):

    fits_files = []
    file_prefix = fits_file.split('image.fits')[0]
    file_prefix = file_prefix.split('-00')[0]
    if nchans > 1:
        for chan in range(1, nchans):
            #zeroth channel flagged
            chan_file = file_prefix + '-{}-image.fits'.format(str(chan).zfill(4))
            fits_files.append(chan_file)
    else:
        fits_files=[fits_file]
    helio_maps = []
    for fits_file in fits_files:
        helio_map = icrs_to_helio(fits_file)
        mask = np.ma.masked_less(helio_map.data, np.nanmax(helio_map.data)/5)
        helio_map.mask = mask.mask
        helio_map.plot_settings['cmap'] = cmap
        helio_maps.append(helio_map)

    return helio_maps

def plot_multifreq(fits_files, stokesI, nchans=4, output=None):
    """
    Does the heavy lifting.
    This is run in parallel per time step and 
    _should_ be able to overplot multiple subbands at the same time.
    """
    if output is None:
        # this needs to be changed manually
        output = '/data/mpearse/radio_aia_images/2024-03-10/multi/' + fits_files[0].stem +'_aia_overlay_no_lasco_multifreq.png'
        if os.path.isfile(output):
            print("File {} exists, quitting without plotting image.".format(output))
            return
    helio_maps = []
    cmaps = ['Blues', 'Greens', 'Oranges', 'PuRd']
    for fits_file, cmap in zip(fits_files, itertools.cycle(cmaps)):
        print("getting helio map {}".format(fits_file.as_posix()))
        helio_maps.append(get_heliomaps(fits_file.as_posix(), nchans=nchans, cmap=cmap))
    helio_map = helio_maps[0][0]
    
    # uncomment if you want to download aia files. ususally best to download them before hand
    # results = Fido.search(a.Time(helio_map.date-1*u.min, helio_map.date+1*u.min, near=helio_map.date),
    #                       a.Instrument.aia, a.Physobs.intensity,
    #                       a.Wavelength(channel*u.angstrom))
    # aia_download = Fido.fetch(results[0][0], path='/data/mpearse/sunpy_data/2022-11-11', overwrite=False)
    
    # change this to where your aia files are.
    aia_list = glob.glob("/data/mpearse/sunpy_data/2024-03-10/aia*.fits") #/aia*lev1
    # uncomment if you want to update the aia image with the closest time to the nenufar one.
    # aia_list.sort()
    # ts = []
    # for af in aia_list:
    #     p = Path(af)
    #     fname = p.name
    #     t = fname[14:36]
    #     d,t = t.split('t')
    #     d = d.split('_')
    #     t = t.split('_')
    #     dt = d+t
    #     dt = [int(i) for i in dt]
    #     ts.append(datetime.datetime(*dt))
    
    # ts = Time(ts)
    # i = np.argmin(np.abs(ts - helio_map.date))
    aia_map0 = sunpy.map.Map(aia_list[0])
    i=0
    aia_map = sunpy.map.Map(aia_list[i])
    norm_data = aia_map.data/aia_map.meta['exptime']
    aia_map1 = sunpy.map.Map(norm_data, aia_map.meta)
    aia_map1.plot_settings['norm'] = ImageNormalize(vmin=aia_map0.min(),
                                                   vmax=aia_map0.max(),
                                                   stretch=LogStretch())

    c3_hv = hv.download_jp2(aia_map.date,
                             observatory='SOHO',
                             instrument='LASCO',
                             detector='C3')
    c2_hv = hv.download_jp2(aia_map.date,
                             observatory='SOHO',
                             instrument='LASCO',
                             detector='C2')
    c3_map = sunpy.map.Map(c3_hv)
    c2_map = sunpy.map.Map(c2_hv)
    background = np.nanmedian(stokesI.db, axis=0)
    plot_stokes = np.log10(stokesI.amp/np.nanpercentile(stokesI.amp, 5, axis=0)) # (stokesV.amp/stokesI.amp).T#
    vmin, vmax = np.nanpercentile(plot_stokes, [5,95]) #[-1,1] #
    
    fig = plt.figure(figsize=(10,10))
    gs = GridSpec(2, 1, height_ratios=(4,1),
                hspace=0.2)
    
    dsax = fig.add_subplot(gs[1])

    im = dsax.pcolormesh(stokesI.time.datetime,
                         stokesI.freq.to(u.MHz).value,
                         plot_stokes.T,
                         shading='nearest',
                         cmap="viridis",
                         vmin=vmin, vmax=vmax)
    dsax.autoscale(axis="x", tight=True)

    dsax.set_xlabel("Time {} UTC".format(helio_map.date.isot[:10]))
    dsax.set_ylabel("Frequency (MHz)")
    #dsax.invert_yaxis()
    dsax.axvline(helio_map.date.datetime, color='white', alpha=0.7)
    cbar = fig.colorbar(im, ax=dsax)
    cbar.set_label('Log intensity (arbitrary)')
 
    # TODO: put this as command line arg
    # if True, include LASCO image in plot
    LASCO = False
    if LASCO:
        with Helioprojective.assume_spherical_screen(aia_map1.observer_coordinate):
            aia_map = aia_map1.reproject_to(c3_map.wcs)
        #pdb.set_trace()
        with Helioprojective.assume_spherical_screen(c2_map.observer_coordinate):
            c2repj_map = c2_map.reproject_to(c3_map.wcs)
        
    #with Helioprojective.assume_spherical_screen(helio_map.observer_coordinate):
    #    heliorepj_map = helio_map.reproject_to(c3_map.wcs)



        observer_map = c3_map
    else:        
        observer_map = aia_map

    ax = fig.add_subplot(gs[0],projection=observer_map)
    with frames.Helioprojective.assume_spherical_screen(observer_map.observer_coordinate):
        if LASCO:
            c3_map.plot(axes=ax, title='')
            c2repj_map.plot(axes=ax, title='')
        aia_map.plot(axes=ax, title='')
        coord_cs = ['#006BA4',
                    '#FF800E',
                    '#ABABAB',
                    '#595959',
                    '#5F9ED1',
                    '#C85200',
                    '#898989',
                    '#A2C8EC',
                    '#FFBC79',
                    '#CFCFCF']
        if nchans == 1:
            for SB, coord_c in zip(helio_maps, itertools.cycle(coord_cs)):
                coord_markers=['o', '^', 's', 'v', 'P']
                for helio_map, marker in zip(SB, itertools.cycle(coord_markers)):
                    lmax = np.nanmax(helio_map.data)
                    levels = [0.5*lmax]#lmax*np.arange(0.5, 1.1, 0.1)
                    contours = ax.contour(helio_map.data,
                                        levels=levels,
                                        colors=coord_c,
                                        vmin=0,
                                        transform=ax.get_transform(helio_map.wcs))
                    max_pix = np.where(helio_map.data == np.nanmax(helio_map.data))*u.pix
                    max_coord = helio_map.pixel_to_world(max_pix[1,0], max_pix[0,0])
                    max_val = np.round(np.nanmax(helio_map.data),2)
                    ax.plot_coord(max_coord, marker, color=coord_c,
                                label=r'{} $T_b$ {:.2E} K'.format(np.round(helio_map.wavelength, 2), max_val))
                    dsax.hlines(helio_map.wavelength.value,
                                stokesI.time[0].datetime, stokesI.time[-1].datetime,
                                color='white', alpha=0.7)
                beam_label = "Beam {}".format(np.round(helio_map.wavelength,2))
                beam_coords_world = SkyCoord(Tx=2000*u.arcsec, Ty=-2000*u.arcsec, frame = observer_map.coordinate_frame)
                beam_coords_pix = observer_map.world_to_pixel(beam_coords_world)
                bx, by = beam_coords_pix.x.value, beam_coords_pix.y.value
                solar_PA = sunpy.coordinates.sun.P(helio_map.date).deg
                b = Ellipse((bx, by),
                        Angle(helio_map.meta['BMAJ'] * u.deg).arcsec / abs(observer_map.scale[0].value) ,
                        Angle(helio_map.meta['BMIN'] * u.deg).arcsec / abs(observer_map.scale[1].value),
                            angle=(90 + helio_map.meta['BPA']) - solar_PA, fill=False,
                            color=coord_c, ls='--')#, label=beam_label)
                ax.add_patch(b)

        else:
            cmap_iter = iter(plt.cm.viridis(np.linspace(0, 1, nchans)))
            freq_list = []
            for helio_map in helio_maps[0]:
                coord_c = matplotlib.colors.rgb2hex(next(cmap_iter), keep_alpha=True)
                lmax = np.nanmax(helio_map.data)
                levels = [lmax*0.5]
                contours = ax.contour(helio_map.data,
                                        levels=levels,
                                        colors=coord_c,
                                        vmin=0,
                                        transform=ax.get_transform(helio_map.wcs))
                # ax.clabel(contours, inline=True, fontsize=10)
                freq_list.append(helio_map.wavelength.to(u.MHz).value)
                max_pix = np.where(helio_map.data == np.nanmax(helio_map.data))*u.pix
                max_coord = helio_map.pixel_to_world(max_pix[1,0], max_pix[0,0])
                max_val = np.round(np.nanmax(helio_map.data),2)
                ax.plot_coord(max_coord, "o", color=coord_c)
                            #   label=r'{} $T_b$ {:.2E} K'.format(np.round(helio_map.wavelength, 2), max_val))
                dsax.hlines(helio_map.wavelength.value,
                            0, 1, transform=dsax.get_yaxis_transform(),
                            # stokesI.time[0].datetime,
                            # stokesI.time[-1].datetime,
                            color='white',
                            alpha=0.1)
                beam_label = "Beam {}".format(np.round(helio_map.wavelength,2))
                beam_coords_world = SkyCoord(Tx=-5000*u.arcsec, Ty=4500*u.arcsec, frame = observer_map.coordinate_frame)
                beam_coords_pix = observer_map.world_to_pixel(beam_coords_world)
                bx, by = beam_coords_pix.x.value, beam_coords_pix.y.value
                solar_PA = sunpy.coordinates.sun.P(helio_map.date).deg
                b = Ellipse((bx, by),
                        Angle(helio_map.meta['BMAJ'] * u.deg).arcsec / abs(observer_map.scale[0].value) ,
                        Angle(helio_map.meta['BMIN'] * u.deg).arcsec / abs(observer_map.scale[1].value),
                            angle=(90 + helio_map.meta['BPA']) - solar_PA, fill=False, color=coord_c,
                            ls='--', label=beam_label)
                ax.add_patch(b)
    if nchans >1:
        cmap = plt.cm.viridis
        # norm = matplotlib.colors.Normalize(vmin=freq_list[0], vmax=freq_list[-1])
        norm = matplotlib.colors.BoundaryNorm(freq_list, cmap.N)
        fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax)
    ax.set_facecolor('black')
    if LASCO:
        xlims_world = [-7500,7500]*u.arcsec
        ylims_world = [-7500,7500]*u.arcsec
        text_pos_world = [-5000, -5000]*u.arcsec
    else:
        xlims_world = [-4000,4000]*u.arcsec
        ylims_world = [-4000,4000]*u.arcsec
        text_pos_world = [-2500, -2500]*u.arcsec
    world_coords = SkyCoord(Tx=xlims_world, Ty=ylims_world, frame=aia_map.coordinate_frame)
    pixel_coords = aia_map.world_to_pixel(world_coords)

    xlims_pixel = pixel_coords.x.value
    ylims_pixel = pixel_coords.y.value

    ax.set_xlim(xlims_pixel)
    ax.set_ylim(ylims_pixel)
    
    text_world_coords = SkyCoord(Tx = text_pos_world[0],
                                 Ty = text_pos_world[1],
                                 frame=aia_map.coordinate_frame)
    text_pixel_coords = aia_map.world_to_pixel(text_world_coords)
    ax.text(text_pixel_coords[0].value, text_pixel_coords[1].value,
            'NenuFAR: {}\nAIA {:.0f} {}: {}'.format(
                helio_map.date.isot[:-4],
                aia_map0.wavelength.value,  r'$\AA$',
                aia_map1.date.isot[:-4]),
            color = 'white',
            fontsize='large'
            )
    if nchans==1:
        ax.legend(loc="upper left")
    plt.tight_layout()
    print("saving to {}".format(output))
    plt.savefig(output)
    fig.clf()
    plt.close('all')
    del fig, ax
    gc.collect()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Produce radio image overlay')
    parser.add_argument('fits_file',nargs='+', help='Name of NenuFAR image fits file.')
    parser.add_argument('-s', '--spectra_files', nargs='+', help='Name of NenuFAR dynamic spectra files.')
    parser.add_argument('--trange', nargs=2, help='Time range for dynamic spectrum in plot')
    parser.add_argument('--frange', nargs=2, help='Frequency range for dynamic spectrum in plot', type=float)
    parser.add_argument('-c', '--channel', type=int, help='Which AIA channel to plot default = 193', default=193)
    parser.add_argument('-n', '--nchannels', type=int,
                        default=1,
                        help='number of frequency channels for a given time')
    parser.add_argument('-o', '--output', default=None, help='Name of output file.')
    parser.add_argument('-a', '--all',
                        action='store_true',
                        help='plot all images with a similar file name.')

    args = parser.parse_args()
    
    fits_file = args.fits_file
    output = args.output
    channel = args.channel
    trange = args.trange
    frange = args.frange
    ds_files = args.spectra_files
    all_plot = args.all
    nchans = args.nchannels
    # Technically you can plot multiple channels from the one subband.
    # I recommend against it because the code is too buggy for nchans > 1.

    if __name__ == "__main__":
        
        print("loading spectra")
        ds = Dynspec(lanefiles=[*ds_files])
        print("setting spectra parameters")
        ds.time_range = trange

        ds.bp_correction = 'standard'
        #ds.jump_correction = True
        ds.rebin_dt = 0.2*u.s
        ds.rebin_df = 195.3125*u.kHz

        """
        Hacky way to combine spectra files. Assume no errors
        find a way to do this properly. 
        Best approach is to only plot a narrow frequency range.
        """
        # if not ((frange[0]*u.MHz >= ds.lanes[0].fmin and frange[1]*u.MHz <= ds.lanes[0].fmax) or \
        #    (frange[0]*u.MHz >= ds.lanes[1].fmin and frange[1]*u.MHz <= ds.lanes[1].fmax)):
        #     ds.freq_range = [ds.lanes[0].fmin, ds.lanes[0].fmax]
        #     stokesI0 = ds.get(stokes='I')
        #     ds.freq_range = [ds.lanes[1].fmin, ds.lanes[1].fmax]
        #     stokesI1 = ds.get(stokes='I')
        #     stokesI_conc = np.concatenate((stokesI0.data, stokesI1.data), axis=1)
        #     freq_conc = np.concatenate((stokesI0.freq, stokesI1.freq))
        #     stokesI = SData(data=stokesI_conc, time=stokesI0.time, freq=freq_conc,polar=['I'])
        
        ds.freq_range = [frange[0]*u.MHz, frange[1]*u.MHz]
        stokesI = ds.get(stokes='I')
        # print(stokesI.amp.shape)
        # stokesV = ds.get(stokes='V')
       
        
        if not all_plot:
            fits_files_freqs = []
            for ff in fits_file:
                f = Path(ff)
                fits_files_freqs.append(f)

            fits_files_freqs = np.array(fits_files_freqs, dtype=object).T
            print("Begin plot")
            plot_multifreq(fits_files_freqs, stokesI, nchans=nchans, output=output)
            plt.show()
        else:
            """
            Makes wild assumptions about fits file name formats
            e.g. file_prefix-t0000-0000-image.fits
            """
            fits_files_freqs = []
            outputs = []
            for ff in fits_file:
                f = Path(ff)
                if nchans > 1:
                    times = list(f.parent.glob(f.name.split('-')[0]+'*-0000-image.fits'))
                else:
                    times = list(f.parent.glob(f.name.split('-')[0]+'*t[0-9][0-9][0-9][0-9]-image.fits'))
                times.sort()
                fits_files_freqs.append(times)
                
                for t in times:
                    
                    output = t.as_posix().replace('radio_images','radio_aia_images').replace('fits', 'png')
                    
                    outputs.append(output)
            fits_files_freqs = np.array(fits_files_freqs, dtype=object).T
            matplotlib.use('Agg')

            with Pool() as pool:
                pool.starmap(plot_multifreq, zip(fits_files_freqs, itertools.repeat(stokesI), itertools.repeat(nchans)))

