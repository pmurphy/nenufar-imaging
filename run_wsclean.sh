#!/bin/bash

./generic_wsclean.sh 1024 2.20 /data/mpearse/radio_images/SB106/wsclean /data/mpearse/SUN-19-5-2022/SB106_aw_av.MS/
#Size:579.5014648437501 Scale:3.9046441675616674 arcmin
#SUN-19-5-2022/SB106_aw_av.MS/

./generic_wsclean.sh 1024 1.48 /data/mpearse/radio_images/SB158/wsclean /data/mpearse/SUN-19-5-2022/SB158_aw_av.MS/
#SUN-19-5-2022/SB158_aw_av.MS/
#Size:579.50146484375 Scale:2.6195714035540303 arcmin

./generic_wsclean.sh 1024 0.91 /data/mpearse/radio_images/SB256/wsclean /data/mpearse/SUN-19-5-2022/SB256_aw_av.MS/
#SUN-19-5-2022/SB256_aw_av.MS/
#Size:579.5014648437501 Scale:1.616766725631003 arcmin

./generic_wsclean.sh 1024 0.75 /data/mpearse/radio_images/SB311/wsclean /data/mpearse/SUN-19-5-2022/SB311_aw_av.MS/
#SUN-19-5-2022/SB311_aw_av.MS/
#Size:579.50146484375 Scale:1.3308433497155523 arcmin

./generic_wsclean.sh 1024 0.65 /data/mpearse/radio_images/SB362/wsclean /data/mpearse/SUN-19-5-2022/SB362_aw_av.MS/
#SUN-19-5-2022/SB362_aw_av.MS/
#Size:579.5014648437499 Scale:1.1433488446451294 arcmin
