#!/bin/bash

./generic_wsclean.sh 512 3.00 /data/mpearse/radio_images/2022-07-11/SB156/typeIII_calbefore /data/mpearse/SUN-2022-07-11/SB156_full.MS/

./generic_wsclean.sh 512 1.80 /data/mpearse/radio_images/2022-07-11/SB260/typeIII_calbefore /data/mpearse/SUN-2022-07-11/SB260_full.MS/

./generic_wsclean.sh 512 1.51 /data/mpearse/radio_images/2022-07-11/SB310/typeIII_calbefore /data/mpearse/SUN-2022-07-11/SB310_full.MS/

./generic_wsclean.sh 512 1.31 /data/mpearse/radio_images/2022-07-11/SB358/typeIII_calbefore /data/mpearse/SUN-2022-07-11/SB358_full.MS/

./generic_wsclean.sh 512 1.30 /data/mpearse/radio_images/2022-07-11/SB360/typeIII_calbefore /data/mpearse/SUN-2022-07-11/SB360_full.MS/

./generic_wsclean.sh 512 1.28 /data/mpearse/radio_images/2022-07-11/SB365/typeIII_calbefore /data/mpearse/SUN-2022-07-11/SB365_full.MS/