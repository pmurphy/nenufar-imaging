#!/bin/bash

./generic_wsclean.sh 1024 2.20 /data/mpearse/radio_images/2022-10-04/SB106/combcal /data/mpearse/SUN-2022-10-04/SB106.MS/
#Size:579.5014648437501 Scale:3.9046441675616674 arcmin
#SUN-2022-10-04/SB106.MS/

./generic_wsclean.sh 1024 1.48 /data/mpearse/radio_images/2022-10-04/SB158/combcal /data/mpearse/SUN-2022-10-04/SB158.MS/
#SUN-2022-10-04/SB158.MS/
#Size:579.50146484375 Scale:2.6195714035540303 arcmin

./generic_wsclean.sh 1024 0.9 /data/mpearse/radio_images/2022-10-04/SB260/combcal /data/mpearse/SUN-2022-10-04/SB260.MS/
#SUN-2022-10-04/SB256.MS/
#Size:579.50146484375 Scale:1.5918933913905262 arcmin
./generic_wsclean.sh 1024 0.75 /data/mpearse/radio_images/2022-10-04/SB311/combcal /data/mpearse/SUN-2022-10-04/SB311.MS/
#SUN-2022-10-04/SB311.MS/
#Size:579.50146484375 Scale:1.3308433497155523 arcmin

./generic_wsclean.sh 1024 0.65 /data/mpearse/radio_images/2022-10-04/SB362/combcal /data/mpearse/SUN-2022-10-04/SB362.MS/
#SUN-2022-10-04/SB362.MS/
#Size:579.5014648437499 Scale:1.1433488446451294 arcmin
