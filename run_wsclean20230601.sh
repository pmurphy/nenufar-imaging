#!/bin/bash

./generic_wsclean.sh 512 3.04 /data/mpearse/radio_images/2023-06-01/SB154/spikes /data/mpearse/SUN-2023-06-01/SB154.MS/

./generic_wsclean.sh 512 3.02 /data/mpearse/radio_images/2023-06-01/SB155/spikes /data/mpearse/SUN-2023-06-01/SB155.MS/

./generic_wsclean.sh 512 3.00 /data/mpearse/radio_images/2023-06-01/SB156/spikes /data/mpearse/SUN-2023-06-01/SB156.MS/

./generic_wsclean.sh 512 2.98 /data/mpearse/radio_images/2023-06-01/SB157/spikes /data/mpearse/SUN-2023-06-01/SB157.MS/

./generic_wsclean.sh 512 2.96 /data/mpearse/radio_images/2023-06-01/SB158/spikes /data/mpearse/SUN-2023-06-01/SB158.MS/

./generic_wsclean.sh 512 2.95 /data/mpearse/radio_images/2023-06-01/SB159/spikes /data/mpearse/SUN-2023-06-01/SB159.MS/

./generic_wsclean.sh 512 2.93 /data/mpearse/radio_images/2023-06-01/SB160/spikes /data/mpearse/SUN-2023-06-01/SB160.MS/

./generic_wsclean.sh 512 2.91 /data/mpearse/radio_images/2023-06-01/SB161/spikes /data/mpearse/SUN-2023-06-01/SB161.MS/
