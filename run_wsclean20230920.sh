#!/bin/bash

./generic_wsclean.sh 512 3.00 /data/mpearse/radio_images/2023-09-20/SB156/typeII_cal2 /data/mpearse/SUN-2023-09-20/SB156_full.MS/

./generic_wsclean.sh 512 1.81 /data/mpearse/radio_images/2023-09-20/SB259/typeII_cal2 /data/mpearse/SUN-2023-09-20/SB259_full.MS/

./generic_wsclean.sh 512 1.51 /data/mpearse/radio_images/2023-09-20/SB310/typeII_cal2 /data/mpearse/SUN-2023-09-20/SB310_full.MS/

./generic_wsclean.sh 512 1.30 /data/mpearse/radio_images/2023-09-20/SB360/typeII_cal2 /data/mpearse/SUN-2023-09-20/SB360_full.MS/
