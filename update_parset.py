#!/usr/bin/env python

"""
Auto generates parset files for DPPP and then runs them
It's a bit of a mess because I try to account for
calibration observations before and after the solar observation
and then combine them.
Calibration MS files should be copied to a local directory.
Solar MS files will be copied from /databf/../ to a local directory.
"""

from pathlib import Path

import argparse
import os

from sunpy.time import TimeRange

import losoto
import losoto.operations
from losoto.h5parm import h5parm

parser = argparse.ArgumentParser(description='creates parset files necessary \
                                 for solar radio imaging with LOFAR')

parser.add_argument('msin', help='input name of Measurement Set.', metavar='MSIN')
parser.add_argument('-t', '--trange', help='Time range over which to perform \
                    calibration. 2 arguments, must be of format YYYY-MM-DDTHH:MM:SS',
                    nargs=2, metavar=('START', 'END'))
parser.add_argument('-c', '--calibrator', help='file is a calibrator MS. Default=False', action='store_true')
parser.add_argument('--combine', help='combine calibrator solutions. Default=False', action='store_true')
parser.add_argument('-r', '--raw', help='file is raw data. Default=False', action='store_true')
parser.add_argument('-f', '--flag', help='run flag step. Default=False', action='store_true')
parser.add_argument('-u', '--undo_aoflag', help='unflag aoflagged data. Default=False', action='store_true')
parser.add_argument('--flag_station', help='name of station to preflag. Default=None', default=None, nargs='+')
parser.add_argument('-d', '--dummy', help='doesn\'t run DP3. Default=False', action='store_true')

args = parser.parse_args()
msin = args.msin
trange = args.trange
flag = args.flag
flag_station = args.flag_station
cal = args.calibrator
combine = args.combine
raw = args.raw
undo_aoflag = args.undo_aoflag
dummy = args.dummy

if trange is not None:
    trange = TimeRange(trange[0], trange[1])
    starttime = trange.start.datetime.strftime("%d%b%Y/%H:%M:%S.%f")
    endtime = trange.end.datetime.strftime("%d%b%Y/%H:%M:%S.%f")
    starttime = starttime[:-3]
    endtime = endtime[:-3]
elif trange is None:
    starttime = ' '
    endtime = ' '

if cal:
    parmdb = msin.replace('.MS', '_instrument.h5') # fails with trailing /
    if flag_station is not None:
        flag_string = [baseline+'&&*' for baseline in flag_station]
        if len(flag_string) > 1:
            flag_string = ';'.join(flag_string)
        else:
            flag_string = flag_string[0]
        with open('/data/mpearse/parsets/auto/cal_preflag.parset', 'w') as f:
            f.write('msin={} \
                    \nmsout=. \
                    \nsteps=[flag] \
                    \nflag.type=preflagger \
                    \nflag.baseline={}\n'.format(msin, flag_string))
        
        os.system('echo Running preflag on calibrator')
        if not dummy:
            os.system('DP3 /data/mpearse/parsets/auto/cal_preflag.parset') 
    
    with open('/data/mpearse/parsets/auto/cal_aoflag.parset', 'w') as f:
        if raw:
            f.write('msin={} \
                    \nmsout=. \
                    \nmsin.starttime={} \
                    \nmsin.endtime={} \
                    \nmsin.autoweight=True \
                    \nmsin.startchan = nchan/16 \
                    \nmsin.nchan = nchan*14/16 \
                    \nsteps=[flag, avg] \
                    \nflag.type=aoflagger \
                    \nflag.strategy=/cep/lofar/DP3/share/rfistrategies/LOFAR-LBA-default.rfis \
                    \navg.type=average \
                    \navg.freqstep=16\n'.format(msin, starttime, endtime))
        else:
            f.write('msin={} \
                    \nmsout=. \
                    \nmsin.starttime={} \
                    \nmsin.endtime={} \
                    \nsteps=[flag] \
                    \nflag.type=aoflagger\n'.format(msin, starttime, endtime))
    
    os.system('echo Running aoflag on calibrator')
    if not dummy:
        os.system('DP3 /data/mpearse/parsets/auto/cal_aoflag.parset')

    with open('/data/mpearse/parsets/auto/sun_cal.parset', 'w') as f:
        f.write('msin={} \
                \nmsout=. \
                \nmsin.starttime={} \
                \nmsin.endtime={} \
                \nsteps=[gaincal] \
                \ngaincal.sourcedb=/data/mpearse/calibrators/Cas_A_LBA.sourcedb \
                \ngaincal.parmdb={} \
                \ngaincal.solint=1 \
                \ngaincal.maxiter=100 \
                \ngaincal.nchan=1 \
                \ngaincal.caltype=diagonal\n'.format(msin, starttime, endtime, parmdb))

    os.system('echo calibrating data')
    if not dummy:
        os.system('DP3 /data/mpearse/parsets/auto/sun_cal.parset')
    
    
    sb_path = Path(msin).as_posix().split('.')[0]
    sb = Path(msin).name.split('.')[0]
    if combine:
        os.system('echo combining parmdbs')
        parmdb = Path('{}_comb_instrument.h5'.format(sb_path))
        if not parmdb.exists():
            if not dummy:
                parmdbs = list(Path(msin).parent.glob('{}*_instrument.h5'.format(sb)))
                os.system('H5parm_collector.py {} {} -o {}'.format(*parmdbs, parmdb))
        amp_prefix = Path(msin).parent/'cal_sols/{}_comb_amp'.format(sb)
        phase_prefix = Path(msin).parent/'cal_sols/{}_comb_phase'.format(sb)
    else:
        parmdb = Path('{}_instrument.h5'.format(sb_path))
        amp_prefix = Path(msin).parent/'cal_sols/{}_amp'.format(sb)
        phase_prefix = Path(msin).parent/'cal_sols/{}_phase'.format(sb)
    H = h5parm(parmdb.as_posix(), readonly=False)
    amp_soltab = H.getSolset('sol000').getSoltab('amplitude000', useCache=True)
    phase_soltab = H.getSolset('sol000').getSoltab('phase000', useCache=True)

    #flag amplitude not working for non-raw data
    if raw:
        axes_to_flag = ['time', 'freq']
        order = [0, 0]
    else:
        axes_to_flag = ['time']
        order = [0]
    # if not dummy:
    #     losoto.operations.flag.run(amp_soltab, axes_to_flag, order)

    #plot before smooth
    if raw:
        axes_in_plot = ['time', 'freq']
    else:
        axes_in_plot = ['time', 'freq']

    axis_in_table = 'ant'
    axis_in_col = 'pol'
    plot_flag = True
    make_ant_plot = False
    do_Unwrap = False
    if not dummy:
        losoto.operations.plot.run(amp_soltab,
            axes_in_plot,
            axis_in_table,
            axis_in_col,
            doUnwrap=do_Unwrap,
            makeAntPlot=make_ant_plot,
            prefix=amp_prefix.as_posix())
    
    do_Unwrap = True
    if not dummy:
        losoto.operations.plot.run(phase_soltab,
            axes_in_plot,
            axis_in_table,
            axis_in_col,
            doUnwrap=do_Unwrap,
            makeAntPlot=make_ant_plot,
            prefix=phase_prefix.as_posix())
    if combine:
        # only smooth if combined calibration solutions
        #smooth amplitude in time
        size = [5]
        axes_to_smooth = ['time']
        mode = 'savitzky-golay'
        degree = 3
        replace = True
        log = True
        if not dummy:
            losoto.operations.smooth.run(amp_soltab, axes_to_smooth, size, mode, degree, replace, log)

        #smooth phase in time
        
        if not dummy:
            mode = 'runningmedian'
            losoto.operations.smooth.run(phase_soltab, axes_to_smooth, size, mode, replace=replace)
            # mode = 'median'
            # losoto.operations.smooth.run(phase_soltab, axes_to_smooth, size, mode, replace=replace)
        #smooth phase in frequecny?

        #plot after smooth
        if raw:
            axes_in_plot = ['time', 'freq']
        else:
            axes_in_plot = ['time', 'freq']
        
        axis_in_table = 'ant'
        axis_in_col = 'pol'
        plot_flag = True
        make_ant_plot = False
        do_Unwrap = False
        amp_prefix = Path(msin).parent/'cal_sols/{}_comb_smooth_amp'.format(sb)
        phase_prefix = Path(msin).parent/'cal_sols/{}_comb_smooth_phase'.format(sb)
        if not dummy:
            losoto.operations.plot.run(amp_soltab,
                axes_in_plot,
                axis_in_table,
                axis_in_col,
                doUnwrap=do_Unwrap,
                makeAntPlot=make_ant_plot,
                prefix=amp_prefix.as_posix())
        
        do_Unwrap = True
        if not dummy:
            losoto.operations.plot.run(phase_soltab,
                axes_in_plot,
                axis_in_table,
                axis_in_col,
                doUnwrap=do_Unwrap,
                makeAntPlot=make_ant_plot,
                prefix=phase_prefix.as_posix())

else:
    if raw:
        with open('sun_autoweight.parset', 'w') as f:
            f.write('msin={} \
                    \nmsout=. \
                    \nsteps=[] \
                    \nmsin.autoweight=True \
                    \nmsin.startchan = nchan/16 \
                    \nmsin.nchan = nchan*14/16\n'.format(msin))

        os.system('echo Running autoweight on sun')
        if not dummy:
            os.system('DP3 sun_autoweight.parset')


    sb_path = Path(msin).as_posix().split('.')[0]
    sb = Path(msin).name.split('.')[0]
    
    local_root = Path('/data/mpearse/')
    msin_local = local_root/'{}/{}_quiet.MS'.format(trange.start.strftime('SUN-%Y-%m-%d'), sb)
    if combine:
        parmdb = Path(msin_local).parent/'CAL/{}_comb_instrument.h5'.format(sb)
    else:
        parmdb = Path(msin_local).parent/'CAL/{}_instrument.h5'.format(sb)
    with open('/data/mpearse/parsets/auto/sun_copy.parset', 'w') as f:
        f.write('msin={} \
                \nmsout={} \
                \nmsin.starttime={} \
                \nmsin.endtime={} \
                \nsteps=[]\n'.format(msin, msin_local, starttime, endtime))

    os.system('echo copying MS')
    if not dummy:
        os.system('DP3 /data/mpearse/parsets/auto/sun_copy.parset')
    
    if undo_aoflag:
        with open('/data/mpearse/parsets/auto/sun_clear_flag.parset', 'w') as f:
            f.write('msin={} \
                    \nmsout=. \
                    \nsteps=[flag] \
                    \nflag.type=preflagger \
                    \nflag.mode=clear \
                    \nflag.baseline=*&&* \
                    \n'.format(msin_local))
        os.system('echo clearing flags')
        if not dummy:
            os.system('DP3 /data/mpearse/parsets/auto/sun_clear_flag.parset')
    if flag_station is not None:
        flag_string = [baseline+'&&*' for baseline in flag_station]
        if len(flag_string) > 1:
            flag_string = ';'.join(flag_string)
        else:
            flag_string = flag_string[0]
        with open('/data/mpearse/parsets/auto/cal_preflag.parset', 'w') as f:
            f.write('msin={} \
                    \nmsout=. \
                    \nsteps=[flag] \
                    \nflag.type=preflagger \
                    \nflag.baseline={}\n'.format(msin_local, flag_string))
        
        os.system('echo Running preflag on calibrator')
        if not dummy:
            os.system('DP3 /data/mpearse/parsets/auto/cal_preflag.parset') 
    with open('/data/mpearse/parsets/auto/sun_applycal.parset', 'w') as f:
        f.write('msin={} \
                \nmsout=. \
                \nmsin.datacolumn=DATA \
                \nmsout.datacolumn=CORRECTED_DATA \
                \nsteps=[apply] \
                \napply.type=applycal \
                \napply.steps=[apply_amp,apply_phase] \
                \napply.apply_amp.correction=amplitude000 \
                \napply.apply_phase.correction=phase000 \
                \napply.direction=[Main] \
                \napply.parmdb={}\n'.format(msin_local, parmdb))

    os.system('echo applying calibration')
    if not dummy:
        os.system('DP3 /data/mpearse/parsets/auto/sun_applycal.parset')

# NenuFAR beam not yet implemented in DPPP
# with open('sun_applybeam.parset', 'w') as f:
#     f.write('msin={} \
#             \nmsout=. \
#             \nmsin.starttime={} \
#             \nmsin.endtime={} \
#             \nmsin.datacolumn=CORR_NO_BEAM \
#             \nmsout.datacolumn=CORRECTED_DATA \
#             \nsteps=[applybeam] \
#             \napplybeam.updateweights=True\n'.format(sun_aw, starttime, endtime))

# os.system('echo applying beam')
# if not dummy:
#     os.system('DP3 sun_applybeam.parset')
